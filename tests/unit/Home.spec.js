import { shallowMount } from '@vue/test-utils';
import Home from '@/views/Home.vue';

let wrapper = shallowMount(Home);

describe('Home.vue', () => {
	describe('Mount test', () => {
		it('Is it mount', () => {
			expect(wrapper.exists()).toBeTruthy();
		});
	});

	describe('Title and buttons tests', () => {
		it('Is title rendered correctly', () => {
			expect(wrapper.find('#title-home').exists()).toBeTruthy();
		});
		it('Is Add Question button rendered correctly', () => {
			expect(wrapper.find('#addQuestion-home').exists()).toBeTruthy();
		});
		it('Is Start Quiz button rendered correctly', () => {
			expect(wrapper.find('#startQuiz-home').exists()).toBeTruthy();
		});
	});
});
