import { shallowMount } from '@vue/test-utils';
import Add from '@/views/Add.vue';

let wrapper = shallowMount(Add);

describe('Add.vue', () => {
	describe('Mount test', () => {
		it('Is it mount', () => {
			expect(wrapper.exists()).toBeTruthy();
		});
	});

	describe('Component render tests', () => {
		it('Create Question header rendered correctly', () => {
			expect(wrapper.find('#title-add').exists()).toBeTruthy();
		});
		it('Process information rendered correctly', () => {
			expect(wrapper.find('#status-add').exists()).toBeTruthy();
		});
		it('Write your question rendered correctly', () => {
			expect(wrapper.find('#question-add').exists()).toBeTruthy();
		});
		it('Choices rendered correctly', () => {
			expect(wrapper.find('#choiceA-add').exists()).toBeTruthy();
			expect(wrapper.find('#choiceB-add').exists()).toBeTruthy();
			expect(wrapper.find('#choiceC-add').exists()).toBeTruthy();
			expect(wrapper.find('#choiceD-add').exists()).toBeTruthy();
			expect(wrapper.find('#choiceAtext-add').exists()).toBeTruthy();
			expect(wrapper.find('#choiceBtext-add').exists()).toBeTruthy();
			expect(wrapper.find('#choiceCtext-add').exists()).toBeTruthy();
			expect(wrapper.find('#choiceDtext-add').exists()).toBeTruthy();
			expect(wrapper.find('#send-add').exists()).toBeTruthy();
		});
	});
});
