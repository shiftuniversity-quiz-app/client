import { shallowMount } from '@vue/test-utils';
import Quiz from '@/views/Quiz.vue';

let wrapper = shallowMount(Quiz);

describe('Quiz.vue', () => {
	describe('Mount test', () => {
		it('Is it mount', () => {
			expect(wrapper.exists()).toBeTruthy();
		});
	});

	describe('Component render tests', () => {
		it('Quiz App header rendered correctly', () => {
			expect(wrapper.find('#title-quiz').exists()).toBeTruthy();
		});
		it('Process information rendered correctly', () => {
			expect(wrapper.find('#status-quiz').exists()).toBeTruthy();
		});
		it('Send button rendered correctly', () => {
			expect(wrapper.find('#send-quiz').exists()).toBeTruthy();
		});
	});

	describe('Score calculation tests', () => {
		wrapper.vm.scoreCalc('a', 'a', 0);
		expect(1).toBe(wrapper.vm.$data.answers[0]);
	});
});
