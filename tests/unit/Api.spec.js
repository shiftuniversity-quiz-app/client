import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const { getQuestions, postQuestion } = require('../../src/api');
const { getData, postData } = require('../../src/mockData');

const mock = new MockAdapter(axios);

const apiURL = 'http://34.118.80.47';

describe('Api funcitons tests', () => {
	afterAll(() => mock.restore());
	beforeEach(() => mock.reset());

	it('GET questions', async () => {
		mock.onGet(apiURL + '/questions').reply(200, getData);

		const r = await getQuestions(apiURL);

		expect(r.status).toBe(200);
	});

	it('POST questions', async () => {
		mock.onPost(apiURL + '/questions').reply(201);

		const r = await postQuestion(apiURL, postData);

		expect(r.status).toBe(201);
	});
});
