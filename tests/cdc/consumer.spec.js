const { Pact } = require('@pact-foundation/pact');
const chai = require('chai');

const expect = chai.expect;

const { getQuestions, postQuestion } = require('../../src/api');
const { getData, postData } = require('../../src/mockData');

const apiURL = 'http://localhost:8081';

describe('Consumer Tests quiz app API', () => {
	const provider = new Pact({
		port: 8081,
		consumer: 'quiz-app-client',
		provider: 'quiz-app-api',
		pactfileWriteMode: 'update',
	});

	before(() => provider.setup());

	describe('When a request is sent to Quiz API', () => {
		describe('When a GET request is sent to Quiz API', () => {
			before(() => {
				return provider.addInteraction({
					uponReceiving: 'All questions',
					withRequest: {
						path: '/questions',
						method: 'GET',
					},
					willRespondWith: {
						status: 200,
						body: getData,
					},
				});
			});
			it('Will receive the list of questions', async () => {
				await getQuestions(apiURL).then((r) => {
					expect(r.status).to.be.equal(200);
					expect(r.data).to.deep.equal(getData);
				});
			});
		});

		describe('When a POST request is sent to Quiz API', () => {
			before(() => {
				return provider.addInteraction({
					uponReceiving: 'Post a new question', // TODO:bunu sor
					withRequest: {
						path: '/questions',
						method: 'POST',
						body: postData,
					},
					willRespondWith: {
						status: 201,
					},
				});
			});

			it('Will recive the status code 201', async () => {
				postQuestion(apiURL, postData).then((r) => {
					expect(r.status).to.be.equal(201);
				});
			});
		});
	});
	after(() => provider.finalize());
});
