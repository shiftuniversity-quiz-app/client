const axios = require('axios');



const getQuestions = async (apiURL) => {
	try {
		return await axios.get(apiURL + '/questions');
	} catch (e) {
		throw new Error(e.message);
	}
};

const postQuestion = async (apiURL, postData) => {
	try {
		return await axios.post(apiURL + '/questions', postData);
	} catch (e) {
		throw new Error(e.message);
	}
};

module.exports = {
	getQuestions,
	postQuestion,
};
