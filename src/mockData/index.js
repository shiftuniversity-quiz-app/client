const postData = {
	title: 'Bla bla',
	a: 'kayseri',
	b: 'konya',
	c: 'adana',
	d: 'tekirdag',
	answer: 'a',
};

const getData = {
	questions: [
		{
			_id: '1',
			title: 'Bla',
			a: 'kayseri',
			b: 'konya',
			c: 'adana',
			d: 'tekirdag',
			answer: 'a',
			_v: 0,
		},
		{
			_id: '2',
			title: 'Bla bla',
			a: 'kayseri',
			b: 'konya',
			c: 'adana',
			d: 'tekirdag',
			answer: 'a',
			_v: 0,
		},
		{
			_id: '3',
			title: 'Bla bla bla',
			a: 'kayseri',
			b: 'konya',
			c: 'adana',
			d: 'tekirdag',
			answer: 'a',
			_v: 0,
		},
	],
};

module.exports = {
	postData,
	getData,
};
